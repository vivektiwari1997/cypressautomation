export SLACK_WEBHOOK_URL="https://hooks.slack.com/services/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"
mkdir mochareports
mkdir -p cypress/reports/mocha
npm install
npm run runSpecFirefox
npx mochawesome-merge cypress/reports/mocha/*json -o mochareports/test-report.json
npx marge mochareports/test-report.json -o mochareports
mv mochareports/mochareports/test-report.html mochareports/test-report.html
npx cypress-slack-reporter --vcs-provider none --ci-provider none --verbose
cp -R mochareports ../
